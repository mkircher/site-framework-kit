module.exports.tasks = {
  uglify: {
    dist: {
      files: [{
        expand: true,
        src: '**/*.js',
        dest: '<%= paths.dist %>/<%= paths.js %>',
        cwd: '<%= paths.tmp %>/<%= paths.js %>',
        ext: '.min.js'
      },{
        expand: true,
        src: '**/*.js',
        dest: '<%= paths.dist %>/<%= paths.apps %>',
        cwd: '<%= paths.tmp %>/<%= paths.apps %>',
        ext: '.min.js'
      }]
    },
    options: {
      sourceMap: true,
      mangle: true,
      preserveComments: false
    }
  }
};