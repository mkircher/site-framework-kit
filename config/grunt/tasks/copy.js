module.exports.tasks = {
	copy: {
		dev:  {
			files: [
				{ src: [ '<%= paths.fonts %>/**' ], dest: '<%= paths.tmp %>/' },
				{ src: [ '<%= paths.img %>/**' ], dest: '<%= paths.tmp %>/' },
				{ src: [ 'index.html' ], dest: '<%= paths.tmp %>/' }
			]
		},
		dist: {
			files: [
				{
					expand: true,
					src:    '**/*',
					dest:   '<%= paths.dist %>/<%= paths.css %>/',
					cwd:    '<%= paths.tmp %>/<%= paths.css %>'
				}, {
					expand: true,
					src:    '**/*',
					dest:   '<%= paths.dist %>/<%= paths.docs %>/',
					cwd:    '<%= paths.tmp %>/<%= paths.docs %>'
				}, {
					src:  [ '<%= paths.fonts %>/**' ],
					dest: '<%= paths.dist %>/'
				}
			]
		},
		apps: {
			files: [
				{
					expand:  true,
					flatten: true,
					src:     [
						'<%= paths.tmp %>/<%= paths.js %>/**/*',
						'<%= paths.dist %>/<%= files.compiledAppImportFilename %>',
						'!<%= paths.tmp %>/<%= paths.js %>/<%= files.commonFilename %>'
					],
					dest:    '<%= paths.tmp %>/<%= paths.apps %>/alerts-dashboard/',
					cwd:     '<%= paths.appsModules %>/alerts-dashboard/'
				},
				{
					expand:  true,
					flatten: true,
					src:     [
						'<%= paths.tmp %>/<%= paths.js %>/**/*',
						'<%= paths.dist %>/<%= files.compiledAppImportFilename %>',
						'!<%= paths.tmp %>/<%= paths.js %>/<%= files.commonFilename %>'
					],
					dest:    '<%= paths.tmp %>/<%= paths.apps %>/tey-calculator/',
					cwd:     '<%= paths.appsModules %>/tey-calculator/'
				},
				{
					expand:  true,
					flatten: true,
					src:     [ '<%= paths.dist %>/<%= paths.css %>/**/*' ],
					dest:    '<%= paths.tmp %>/<%= paths.css %>/tey-calculator/',
					cwd:     '<%= paths.appsModules %>/tey-calculator/'
				},
				{
					expand: true,
					src:    [
						'<%= paths.dist %>/<%= paths.css %>/**/*',
						'<%= paths.fonts %>/**/*'
					],
					dest:   '<%= paths.tmp %>/',
					cwd:    '<%= paths.appsModules %>/tey-calculator/'
				}
			]
		}
	}
};
