var toastr = require( 'toastr' );

module.exports = Backbone.Model.extend( {
	
	radio: Backbone.Radio.channel( Backbone.Radio._SiteRadioChannelName ),
	
	initialize: function( options ){
		
		this.setupEventListeners();
		this.setupToastr();
	},
	
	setupToastr: function(){
		
		toastr.options = {
			positionClass:     'toast-top-center',
			showMethod:        'fadeIn',
			hideMethod:        'fadeOut',
			closeMethod:       'fadeOut',
			showDuration:      200,
			hideDuration:      400,
			timeOut:           3500,
			extendedTimeOut:   1000,
			preventDuplicates: true,
			progressBar:       false,
			closeButton:       false,
			tapToDismiss:      false,
			newestOnTop:       false
		};
	},
	
	setupEventListeners: function(){
		
		this.radio.reply( {
			'app:message:success':  this._handleSuccessMessageRequest,
			'app:message:error':    this._handleErrorMessageRequest,
			'app:message:warning':  this._handleWarningMessageRequest,
			'app:message:info':     this._handleInfoMessageRequest,
			'app:message:clear':    this._handleMessageClearRequest,
		}, this );
	},
	
	_handleSuccessMessageRequest: function( requestOptions ){
		
		toastr.remove();
		
		requestOptions = _.extend( {
			message:   'Success!'
		}, requestOptions );
		
		toastr.success( requestOptions.message );
	},
	
	_handleErrorMessageRequest: function( requestOptions ){
		
		toastr.remove();
		
		requestOptions = _.extend( {
			message:   'Error!'
		}, requestOptions );
		
		toastr.error( requestOptions.message );
	},
	
	_handleWarningMessageRequest: function( requestOptions ){
		
		toastr.remove();
		
		requestOptions = _.extend( {
			message:   'Warning!'
		}, requestOptions );
		
		toastr.warning( requestOptions.message );
	},
	
	_handleInfoMessageRequest: function( requestOptions ){
		
		toastr.remove();
		
		requestOptions = _.extend( {
			message:   'Info!'
		}, requestOptions );
		
		toastr.info( requestOptions.message );
	},
	
	_handleMessageClearRequest: function(){
		
		toastr.clear();
	}
	
} );