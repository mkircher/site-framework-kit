module.exports.tasks = {
  browserify: {
    apps: {// Src matches are relative to this path
      src:  '<%= paths.js %>/<%= gruntfileConfig.files.mainFileName %>',   // Actual patterns to match, save the common libs file,
      dest: '<%= paths.tmp %>/<%= paths.js %>/<%= gruntfileConfig.files.mainFileName %>'
    },
    vendor: {
      src:  '<%= paths.js %>/<%= gruntfileConfig.files.commonFilename %>',
      dest: '<%= paths.tmp %>/<%= paths.js %>/<%= gruntfileConfig.files.commonFilename %>'
    }
  }
};
