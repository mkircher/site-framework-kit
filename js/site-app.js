/**
 * Main application class. Gets required data and renders the application layout.
 *
 *  @class        Application
 *  @extends      Marionette.Application
 *  @constructor
 */

// Setup radio channel namespace
Backbone.Radio._SiteRadioChannelName = "SiteRadio";

var SiteConfigModel       = require( './models/SiteConfigModel' ),
    SiteDataModel         = require( './models/SiteDataModel' ),
    SiteUserMessageModel  = require( './models/SiteUserMessageModel' );

module.exports = Marionette.Application.extend( {
	
	/**
	 * Backbone radio channelname; used to create a reference
	 * to Backbone Radio from config value
	 *
	 * @property radio
	 * @type Backbone.Radio.channel
	 */
	channelName: Backbone.Radio._SiteRadioChannelName,
	
	/**
	 * The radio object for the application.
	 *
	 * @property radio
	 * @type Backbone.Radio.channel
	 */
	radio: Backbone.Radio.channel( Backbone.Radio._SiteRadioChannelName ),
	
	/**
	 * The configuration object for the application.
	 *
	 * @property config
	 * @type SiteConfigModel
	 */
	config: new SiteConfigModel(),
	
	/**
	 * This method is invoked on the before:start event.
	 * It initializes the Config and App Data object,
	 * and sets the application namespace.
	 * @method onBeforeStart
	 * @private
	 */
	onBeforeStart: function( options ){
		
		// Install app to global space
		this.config.installAppNamespace( this );
	},
	
	/**
	 * This method is invoked on the Marionette.Application start event.
	 * It setsup the config object and then runs views that should be attached to the page.
	 * @method onStart
	 * @private
	 */
	onStart: function( options ){
		
		_.bindAll( this,
			'setupGlobalViews',
			'setupGlobalModels'
		);
		
		// Setup app, then fetch app data
		$.when( this.config.setupAppContext() )
		 //.then( this.config.fetchConfigValues ) // Need to get a config API directory? Uncomment this.
		 .then( this.setupGlobalModels )
		 .then( this.setupGlobalViews );
	},
	
	setupGlobalViews: function(){
		
	},
	
	setupGlobalModels: function(){
		
		this.models = {
			datastore: new SiteDataModel(),
			messages:  new SiteUserMessageModel()
		};
	}
} );