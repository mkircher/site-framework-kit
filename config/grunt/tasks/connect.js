module.exports.tasks = {
  connect:{
    server:{
      options:{
        base: '<%= gruntfileConfig.paths.tmp %>',
        debug: true
      }
    },
    options:{
      liveReload: true,
      port: '<%= gruntfileConfig.connect.port %>',
      hostname: 'localhost',
      open: 'http://localhost:<%= gruntfileConfig.connect.port %>'
    }
  }
};