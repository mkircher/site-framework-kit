module.exports.tasks = {
	juice: {
		options: {
			preserveMediaQueries: true,
			webResources: {
				scripts: false,    // don't fetch external scripts
				images: 0,         // don't inline images with size smaller than this (kb)
			}
		},
		emails:    {
			files: [
				{
					expand: true,
					src:    '**/*',
					cwd:    '<%= gruntfileConfig.paths.tmp %>/emails/',
					dest:   '<%= gruntfileConfig.paths.tmp %>/emails'
				}
			]
		}
	}
};