module.exports = function(lvalue, operator, rvalue, options) {
	return Handlebars.helpers.compare(moment(lvalue), operator, moment(rvalue), options);
};