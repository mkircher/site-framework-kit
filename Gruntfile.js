/*global module:false*/
module.exports = function( grunt ){
	
	// Read in project specific config file (specifies
	// path locations, output filenames, ports to run on, etc.)
	var gruntfileConfig = grunt.file.readJSON( 'config/grunt/gruntfileConfig.json' );
	
	// Load all grunt tasks into memory
	require( 'load-grunt-tasks' )( grunt );
	
	// Config project paths & references
	var options = {
		pkg:             grunt.file.readJSON( 'package.json' ),  // npm packages file
		gruntfileConfig: gruntfileConfig,                      // project's grunt config file
		paths:           gruntfileConfig.paths,                // setup specified project paths from grunt config file,
		files:           gruntfileConfig.files,                // setup specified project paths from grunt config file,
		config:          {
			src: "config/grunt/tasks/*.js"                    // path to where the grunt tasks reside
		}
	};
	
	// Load the various task configuration files with options from above
	var configs = require( 'load-grunt-configs' )( grunt, options );
	
	// Initialize Grunt with configs
	grunt.initConfig( configs );
	
	// Default task.
	grunt.registerTask( 'default', 'Compiles sass, watches for changes. Server is up to you.', [
		'watch'
	] );
	
	grunt.registerTask( 'serve', 'Default setup but runs a connect server for you.', [
		'clean:apps',
		'sass',
		'postcss:dev',
		'browserify',
		'handlebars',
		'compile-handlebars',
		'copy:dev',
		'processhtml:apps',
		'connect',
		'watch'
	] );
	
	grunt.registerTask( 'docs', 'Generate JS, CSS, and SASS documentation', [
		'clean:docs',
		'yuidoc',
		'sassdoc',
		'dss',
		'copy:docs'
	] );
	
	grunt.registerTask( 'write-docs', 'Watch and regen JS, CSS, and SASS documentation', [
		'connect',
		'watch:docs'
	] );
	
	grunt.registerTask( 'build', "Build the site and package up it's components", [
		'clean',
		'sass',
		'postcss:dist',
		'browserify',
		'handlebars',
		'compile-handlebars',
		'juice',
		'processhtml:apps',
		'processhtml:dist',
		'copy:dist',
		'uglify',
		'imagemin'
	] );
	
	grunt.registerTask( 'build-target', 'Sets the build / distribution directory to build into and then builds. grunt build-target:[some directory path]', function( dir, type ){
		
		var paths = grunt.config.get( 'paths' );
		
		if( dir == null ){
			grunt.warn( '\n\n=== Build target directory must be specified first! ===\n|\n|  Command should look like this:\n|  -> grunt build-target:[some directory path]\n|\n' );
		}
		paths.dist = dir;
		grunt.config.set( 'paths', paths );
		
		if( type == null ){
			
			grunt.task.run( 'build' );
			
		} else {
			
			grunt.task.run( 'build:' + type );
		}
	} );
	
	grunt.registerTask( 'build-emails', [
		'compile-handlebars',
		'sass',
		'postcss',
		'juice',
		'processhtml:dist'
	] );
};
