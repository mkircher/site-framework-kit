// Require any common libraries here.
require( 'jquery' );
require( 'underscore' );
require( 'backbone' );
require( 'backbone.radio' );
require( 'marionette' );
require( 'handlebars' );
//require( 'bootstrap' );
require( 'moment' );
require( 'bowser' );
require( 'js-cookie' );

// Register Handlebars handlers
Handlebars.registerHelper( 'compare', require( './lib/handlebars.helpers/compare' ) );
Handlebars.registerHelper( 'formatDate', require( './lib/handlebars.helpers/formatDate' ) );
Handlebars.registerHelper( 'indexOf', require( './lib/handlebars.helpers/indexOf' ) );
Handlebars.registerHelper( 'oneBasedIndex', require( './lib/handlebars.helpers/oneBasedIndex' ) );
Handlebars.registerHelper( 'objectKey', require( './lib/handlebars.helpers/objectKey' ) );
Handlebars.registerHelper( 'compareDates', require( './lib/handlebars.helpers/compareDates' ) );
Handlebars.registerHelper( 'exists', require( './lib/handlebars.helpers/exists' ) );
Handlebars.registerHelper( 'ifIn', require( './lib/handlebars.helpers/ifIn' ) );
Handlebars.registerHelper( 'ifNotIn', require( './lib/handlebars.helpers/ifNotIn' ) );

// Add Polyfills
require('./lib/polyfills').fround();