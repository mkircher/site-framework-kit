var config = Backbone.Model.extend( {
	
	radio: Backbone.Radio.channel( Backbone.Radio._SiteRadioChannelName ),
	
	urlRoot: 'http://www.site.com',
	
	url: 'api/version-xx/directory',
	
	defaults: function(){
		return {
			appContainerEl:   null,
			appNamespace:     "SiteApp",
			restUrls:         {
				//setup in init
			},
			environment:      {
				device: null
			},
			
			// Enums
			enums: {
				userFormats:   {
					nullId: "00000000-0000-0000-0000-000000000000"
				},
				dateFormats:   {
					DATE:      "MM/DD/YYYY",
					DATE_TIME: "MM/DD/YYYY, h:mm:ss a",
					DATA_FULL: "llll"
				}
			}
		}
	},
	
	/**
	 * Initializes the config model
	 *
	 * @method initialize
	 * @return None
	 */
	initialize: function(){
		
		_.bindAll( this,
			'_handleConfigFetchValuesSuccess',
			'_processConfigURLs',
			'fetchConfigValues'
		);
		
		this.setupEventListeners();
		this.determineEnvironmentVariables();
	},
	
	determineEnvironmentVariables: function(){
		
		if( window.location.host.split( ':' )[ 0 ] == 'localhost' ){
			
			this.urlRoot = 'http://' + this.get( 'baseUrl' ) + '/';
			
		} else {
			
			var getUrl   = window.location;
			this.urlRoot = getUrl.protocol + "//" + this.get( 'baseUrl' ) + "/";
		}
	},
	
	/**
	 * Backbone radio setup for this model
	 *
	 * @method setupEventListeners
	 */
	setupEventListeners: function(){
		
		// Requests / reply
		this.radio.reply( {
			'site:info:config':  this._handleConfigObjectRequest,
			'site:info:api:url': this._handleRestUrlObjectRequest,
			'site:info:enums':   this._handleEnumObjectRequest
		}, this );
	},
	
	/**
	 * Private method to get this model, or an attribute of it
	 * @method _handleConfigObjectRequest
	 * @param attribute The model attribute to retrieve
	 * @returns {*} The model itself, or an attribute specified
	 * @private
	 */
	_handleConfigObjectRequest: function( attribute ){
		
		if( _.isString( attribute ) && this.has( attribute ) ){
			return this.get( attribute );
		} else {
			return this;
		}
	},
	
	/**
	 * Private method to get a url endpoint object
	 * @method _handleRestUrlObjectRequest
	 * @param key {String} The key for a particular REST Url (generated from the directory names provided)
	 * @returns {Object} The generated REST Url object
	 * @private
	 */
	_handleRestUrlObjectRequest: function( key ){
		
		try{
			
			return this._parseRestUrl( key );
			
		} catch( e ) {
			
			console.error( 'There is no configured endpoint for "' + key + '", or a parse error occured with the api object' );
		}
	},
	
	/**
	 * Private method to retrieve a config enum
	 * @method _handleEnumObjectRequest
	 * @param type {String} The enum key to retrieve
	 * @returns {*} All the enums in config, or a key set of those enums
	 * @private
	 */
	_handleEnumObjectRequest: function( type ){
		
		try{
			
			if( type ){
				return this.get( 'enums' )[ type ];
			} else {
				return this.get( 'enums' );
			}
			
		} catch( e ) {
			
			console.error( 'There is no enums for "' + type + '"' );
		}
	},
	
	/**
	 * Registers an app's namespace onto the global window object. This
	 * allows the app (and developers) to reference the app in a safe space. Use
	 * in console as window.<APPNAMESPACE>, or simply <APPNAMESPACE>.my.objects, etc.
	 *
	 * @method installAppNamespace
	 * @param AppReference {Backbone.Marionette.Application} Instance of a Marionette App
	 * @return None
	 */
	installAppNamespace: function( AppReference ){
		window[ this.get( 'appNamespace' ) ] = AppReference;
	},
	
	/**
	 * Fetches config values from the server
	 * @method fetchConfigValues
	 * @returns {Object} Deferred ajax object
	 */
	fetchConfigValues: function(){
		
		var url = this.urlRoot + this.url + "?" + $.now();
		return $.get( url ).then( this._handleConfigFetchValuesSuccess );
	},
	
	/**
	 * Handler for a successful config model fetch
	 * @method _handleConfigFetchValuesSuccess
	 * @param data {Object} return from config serve fetch call
	 * @private
	 */
	_handleConfigFetchValuesSuccess: function( data ){
		
		this._processConfigURLs( data );
	},
	
	/**
	 * Processes the api directory into a referrential object off the config model.
	 * Currently just replaces whitespace and lowercases everything.
	 * @param urls {Array} Returned endpoints array returned from the directory
	 * @private
	 */
	_processConfigURLs: function( urls ){
		var restUrls     = {},
		    replaceRegex = /( )/g;
		
		_.each( urls.EndPoints, function( restUrl, idx ){
			
			var name = restUrl.Name.replace( replaceRegex, '' ).toLowerCase();
			
			if( _.isUndefined( restUrls[ name ] ) ){
				restUrls[ name ] = restUrl;
			} else {
				restUrls[ name + restUrl.$id ] = restUrl;
			}
			
		}, this );
		this.set( 'restUrls', restUrls );
	},
	
	/**
	 * @method _parseRestUrl
	 * @param key
	 * @returns Object {{url: string, root: string, endpoint: string, method: string, params: object}}
	 * @private
	 */
	_parseRestUrl: function( key ){
		
		var urlRoot     = this.urlRoot,
		    url         = this.get( 'restUrls' )[ key ].Source,
		    method      = this.get( 'restUrls' )[ key ].Method,
		    urlRegex    = /(\/{\w*})/gi,
		    paramRegex  = /\/{(\w*)}/gi,
		    paramObject = {},
		    urlObject, params;
		
		//get all params in the url as an empty object
		params = url.match( paramRegex );
		
		if( !_.isNull( params ) ){
			params = url.match( paramRegex ).join( ',' ).replace( /(\/|{|})/g, '' ).split( ',' );
			
			_.each( params, function( param ){
				paramObject[ param ] = null;
			}, this );
		}
		
		//create object for consumption
		urlObject = {
			url:      urlRoot + url,
			root:     urlRoot,
			endpoint: url,
			method:   method,
			params:   paramObject
		};
		
		return urlObject;
	},
	
	/**
	 * This method defines the api endpoints the app should use, based on it's environment,
	 * as well as the online status. It then provides the app with ancillary information regarding
	 * the data that was brought down, if necessary.
	 *
	 * @method setupAppContext
	 * @return {Promise} the combined promise of environment, online status, and capabilities
	 * @private
	 */
	setupAppContext: function(){
		
		return $.when(
			this.defineAppEnvironment()
		);
	},
	
	/**
	 * This method determines what API REST urls to use, based on environment discovery. Currently detects
	 * generic web and ios:ipad device usage. Overwrites the entire set of possible maps with the applicable submap.
	 *
	 * @method defineAppEnvironment
	 * @return {Promise} a generic promise contruct used for piping
	 * @private
	 */
	defineAppEnvironment: function(){
		
		var environmentObj = this.get( 'environment' ),
		    thisEnviroment = _.clone( environmentObj ),
		    restUrls       = this.get( 'restUrls' );
		
		thisEnviroment.deviceName = bowser.name;
		thisEnviroment.device     = bowser;
		
		this.set( {
			restUrls:    restUrls,
			environment: _.extend( environmentObj, thisEnviroment )
		} );
		
		return $.when( true );
	}
} );

module.exports = config;
