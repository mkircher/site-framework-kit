module.exports.tasks = {
  watch: {
    html: {
      files: ['<%= paths.tmpl %>/**/*.{html,hbs,json}', '!*.html'],
      tasks: ['compile-handlebars','processhtml:apps']
    },
    styles: {
      files: ['<%= paths.sass %>/**/*.scss'],
      tasks: ['sass','postcss:dev']
    },
    assets: {
      files: ['<%= paths.img %>/**/*', '<%= paths.fonts %>/**/*'],
      tasks: ['copy:dev']
    },
    js:{
      files: [
        '<%= paths.js %>/**/*.js',
        '<%= paths.tmpl %>/**/*.hbs'
      ],
      tasks: [
        'browserify:vendor',
        'browserify:apps',
        'handlebars'
      ]
    },
    docs:{
      files: ['<%= paths.sass %>/**/*.scss',
              '<%= paths.tmpl %>/**/*.{hbs,handlebars}'],
      tasks: ['sassdoc','dss']
    },
    options: {
      //add <script src="//localhost:35729/livereload.js"></script> to html file
      livereload: true
    }
  }
};