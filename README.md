# `Project Name` README #

This repo serves as the development workbench and asset generator for `[project name]`. You can use this codebase as scaffolding for CMS integration, or as a complete solution to `[project name]`'s frontend site / app.

### Who do I talk to? ###

* [YOUR NAME](name@domain.com), Primary Development
* [YOUR NAME](name@domain.com), Support Development

---

### How do I get set up? ###

#### 1. Get Dependencies ####
You will be required to have [Node](https://nodejs.org/en/) installed and be able to run [node package manager](https://www.npmjs.com/package/npm). NPM should come already with Node.

---

#### 2. Install NPM packages ####
To install all required packaged used to build the project, run:

`npm install`

---

#### 3. Get app submodules from external repos ####
To install all apps used in the build pages, use both git submodule commands:

- `git submodule init`, then
- `git submodule update`

Reading the [GIT-scm about submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) may help.

---

#### List Grunt Tasks ####
Each task Grunt can run should be available out of the box. To list all tasks available to you, please run the following command:

`grunt --help`

---

### 3. Configure Your Grunt Setup ###

Open **/configs/grunt/gruntfileConfig.json** and update attributes according to your desired file structures and naming conventions


### Development ###

Run grunt's connect server command to start a watched session

`grunt serve`

This will setup a new connection to your configured port, and open localhost. You can now work on and update and HTML, SASS, or JS files necessary; upon saving the connect server will refresh and reflect the changes.

---

### Documentation ###

For documentation, you can start a watched session to write up your docs in comment blocks for JS, CSS, and SASS files.
You can see an example in the included JS files, and the widget SASS component files.

The following parsers are used:

- **YUIDocs:**
  - Used for ALL JS files
  - Example comment block found in :
    - **/js/site.js**
    - **/js/models/util/config.js**
  - Template and stylesheet is default to YUIDocs
  - [Syntax reference](http://yui.github.io/yuidoc/syntax/)

- **SASSDoc:**
  - Used on SCSS files that ARE mixin, extension, or var files
  - Example comment block found in:
    - **/sass/components/widget/_mixins.scss**
    - **/sass/components/widget/_vars.scss**
  - Template and stylesheet is default to SASSDoc
  - [Syntax reference](http://sassdoc.com/annotations/)

- **DSS Parser:**
  - Used on SCSS files that are NOT mixin, extension, or var files
  - Example comment block found in:
    - **/sass/components/_widget.scss** - Contains all modifier parser elements
  - Style guide template used to create the styleguide HTML, should you want to change it, is found in:
    - **/templates/styleguide/index.handlebars**
  - Style guide stylesheet, should you want to change it, is found in:
    - **/sass/external/styleguide-paper.css**
  - [Syntax overview](https://www.npmjs.com/package/dss)

To write documenation you want to see updated, start a watch session by running:

`grunt write-docs`

To export all docs to the distribution folder (or wherever you have decided to export to within your Grunt config), run:

`grunt docs`

---

### Deployment ###

To build the project into a distribution folder, run the following:

`grunt build`

To specify a target folder / path directory to build into:

`grunt build-target <path>`

This will create a **dist** folder (or whatever you called it in your Grunt configuration) that generates compiled CSS, JS and minified assets (images, fonts, etc).

In detail, the following tasks will occur:

* Clean out old distribution folders
* Compile SASS files
* Run post-CSS tasks
* Split CSS if necessary
* Compile all JS modules
* Compile templates
* Concatenate JS assets into production ready files
* Compress JS
* Compress image sizes
* Compile HTML pages
* Generate CSS, SASS, and JS documenation
