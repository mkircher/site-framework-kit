require( 'backbone-localstorage' );

module.exports = Backbone.Model.extend( {
	
	radio: Backbone.Radio.channel( Backbone.Radio._SiteRadioChannelName ),
	
	defaults: function(){
		return {
			// attributes that hold data go here.
		}
	},
	
	initialize: function(){
		
		this.setupLocalStorage();
		this.setupEventListeners();
		this.fetchInitialData();
	},
	
	setupLocalStorage: function(){
		
		// Do you want local storage for this stuff? Uncomment below and will be stored under the config namespace.
		// Backbone.LocalStorage.setPrefix( this.radio.request( 'site:info:config', 'appNamespace' ) );
	},
	
	setupEventListeners: function(){
		
		// Requests / reply
		this.radio.reply( {
			'site:data:get': this._handleDataObjectRequest
		}, this );
	},
	
	/**
	 * Private method to get this model, or an attribute of it
	 * @method _handleDataObjectRequest
	 * @param attribute The model attribute to retrieve
	 * @returns {*} The model itself, or an attribute specified
	 * @private
	 */
	_handleDataObjectRequest: function( attribute ){
		
		if( _.isString( attribute ) && this.has( attribute ) ){
			return this.get( attribute );
		} else {
			return this;
		}
	},
	
	fetchInitialData: function(){
		
		// Do initial fetching of data here. If you have localstorage on, it will just defer to that.
	}
} );
