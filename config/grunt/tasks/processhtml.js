module.exports.tasks = {
  processhtml: {
    dist: {
      options:{
        commentMarker: 'build',
        process: true,
        strip: true
      },
      files: [{
        expand: true,                                 // Enable dynamic expansion
        cwd:    '<%= paths.tmp %>/',                  // Src matches are relative to this path
        src:    ['**/*.html', '!**/_dev/**/*.html'],  // Actual patterns to match
        dest:   '<%= paths.dist %>/'
      }]
    },
    apps: {
      options:{
        commentMarker: 'appModules',
        process: false,
        strip: false
      },
      files: [{
        expand: true,                 // Enable dynamic expansion
        cwd:    '<%= paths.tmp %>/',  // Src matches are relative to this path
        src:    ['**/*.html'],        // Actual patterns to match
        dest:   '<%= paths.tmp %>/'
      }]
    }
  }
};