module.exports.tasks = {
  concat: {
    '<%= paths.tmp %>/<%= paths.js %>/<%= gruntfileConfig.files.commonFilename %>': [
      '<%= paths.tmp %>/<%= paths.js %>/<%= gruntfileConfig.files.commonFilename %>',
      '<%= paths.tmp %>/<%= paths.js %>/<%= gruntfileConfig.files.compiledTemplatesFilename %>'
    ]
  }
};