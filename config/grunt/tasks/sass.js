module.exports.tasks = {
	sass: {
		default: {
			files: [{
					expand: true,
					src:    [ '**/*.scss' ],
					cwd:    '<%= paths.sass %>',
					dest:   '<%= paths.tmp %>/<%= paths.css %>',
					ext:    '.css'
				}
			]
		}
	}
};