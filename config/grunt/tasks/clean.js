module.exports.tasks = {
  clean: {
    css: {
      src: ["<%= paths.css %>"]
    },
    tmp: {
      src: ["<%= paths.tmp %>"]
    },
    dist: {
      src: ["<%= paths.dist %>"]
    },
    docs: {
      src: ["<%= paths.docs %>"]
    },
    apps: {
      src: ["<%= paths.apps %>"]
    }
  }
};