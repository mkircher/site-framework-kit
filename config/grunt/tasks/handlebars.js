module.exports.tasks = {
  handlebars: {

    options: {
      //provide a namespace for templates
      namespace: '<%= gruntfileConfig.handlebars.namespace %>',

      //remove template path and extention portions from name:
      processName: function(filePath) {
        return filePath.replace(/^templates\//, '').replace(/\.(hbs|handlebars)$/, '');
      }
    },
    all: {
      files: {
        "<%= paths.tmp %>/<%= paths.js %>/<%= gruntfileConfig.files.compiledTemplatesFilename %>": ["<%= paths.tmpl %>/**/*.{hbs,handlebars}"]
      }
    }
  }
};