module.exports.tasks = {
  'compile-handlebars': {
    default:{
      handlebars:   'node_modules/handlebars',
      partials:     '<%= paths.tmpl %>/includes/**/*.hbs',
      templateData: '<%= paths.tmpl %>/pages/**/*.json',
      helpers:      '<%= paths.js %>/lib/handlebars.helpers/**/*.js',
      options: {
        registerFullPath: true,
        globals: [{
          title:  '<%= pkg.title %>',
          domain: '<%= pkg.domain %>'
        }]
      },
      files:[{
        src:        '<%= paths.tmpl %>/pages/**/*.hbs',   // Source files
        dest:       '<%= paths.tmp %>',
        expand:     true,
        flatten:    false,
        cwd:        '.',
        ext:        '.html',
        rename:     function(dest, matchedSrcPath, options) {
  
          // remove 'template/pages' from src path
          var filePath = matchedSrcPath.split('/');
          filePath.shift();
          filePath.shift();
          filePath = dest + '/' + filePath.join('/');
          
          return filePath;
        }
      }]
    }
  }
};
