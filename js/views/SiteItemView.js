module.exports = Marionette.View.extend( {
	
	radio: Backbone.Radio.channel( Backbone.Radio._SiteRadioChannelName ),
	
	template: false,
	
	defaults: {
		// Det defaults for view here.
	},
	
	templates: {
		// Reference other templates used here.
	},
	
	classes: {
		// Reference classes used here.
	},
	
	initialize: function( opts ){
		
		// merge user options into defaults and produce final options
		this.options    = _.extend( this.defaults, opts );
		
		// merge user templates into defaults and produce final templates
		this.templates  = _.extend( this.templates, opts.templates || {} );
		
		this.setupEventListeners();
	},
	
	setupEventListeners: function(){
		
		// Add event listeners here.
	}
} );
