var Application = require( './site-app' );

// start app when ready
$( document ).ready( function(){
	
	// Create App with radio channel
	var App = new Application();
	
	App.start();
	
} );