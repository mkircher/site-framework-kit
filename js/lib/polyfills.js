module.exports = {
	
	fround: function(){
		
		/*https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/fround#Polyfill*/
		Math.fround = Math.fround || (function( array ){
			return function( x ){
				return array[ 0 ] = x, array[ 0 ];
			};
		})( Float32Array( 1 ) );
	}
};