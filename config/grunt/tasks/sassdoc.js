module.exports.tasks = {
  sassdoc: {
    default: {
      src: '<%= paths.sass %>/**/*.scss',
      options: {
        dest: '<%= paths.tmp %>/<%= paths.docs %>/sass/',
        theme: 'default',
        groups: {
          undefined: "General"
        }
      }
    }
  }
};