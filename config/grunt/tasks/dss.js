module.exports.tasks = {
  dss: {
    docs: {
      files: {
        '<%= paths.tmp %>/<%= paths.docs %>/css/': [
          'sass/{components,layouts}/*.{scss,sass}',
          'sass/global/extends/*.{scss,sass}',
          'sass/style.{scss,sass}'
        ]
      },
      options:{
        template: '<%= paths.tmpl %>/styleguide/',
        include_empty_files: false,
        parsers: {
          group: function(i, line, block){
            return {
              name:       line.trim(),
              component:  line.trim().toLowerCase() == 'component',
              layout:     line.trim().toLowerCase() == 'layout',
              global:     line.trim().toLowerCase() == 'global'
            };
          },
          nooutput: function(i, line, block){
            return true;
          },
          base: function(i, line, block){
            return line.substring(1);
          },
          tag: function(i, line, block){
            return line;
          },
          modifier: function(i, line, block){
            var state                   = line.split(' - '),
                modifierRegex           = /(.*)(<.*>)( )(\[.*\])( - )(.*)/,
                internalModifierRegex   = /(\(.*\))( )(.*)/,
                options                 = null,
                optionDesc              = null,
                optionLabel             = null,
                internalModifier        = null,
                parts;

            if ((parts = modifierRegex.exec(line)) !== null) {
              if (parts.index === modifierRegex.lastIndex) {
                modifierRegex.lastIndex++;
              }
              state       = [ parts[1], parts[6] ];
              optionLabel = parts[2];
              optionDesc  = parts[4].substring( 1, parts[4].length - 1 );
              options     = optionDesc.split('|');
            }

            if ((parts = internalModifierRegex.exec(state[1])) !== null) {
              if (parts.index === internalModifierRegex.lastIndex) {
                internalModifierRegex.lastIndex++;
              }
              state[1]         = parts[3];
              internalModifier = parts[1].substring(1, parts[1].length - 1);
            }

            return {
              name:               (state[0]) ? state[0].trim() : '',
              escaped:            (state[0]) ? state[0].trim().replace('.', ' ').replace(':', ' pseudo-class-') : '',
              description:        (state[1]) ? state[1].trim() : '',
              optionLabel:        optionLabel,
              optionDesc:         optionDesc,
              options:            options,
              internalModifier:   internalModifier
            };
          },
          class: function(i,line,block){

            var state                   = line.split(' - '),
                modifierRegex           = /(.*?(<.*>).*)( )(\[.*\])( - )(.*)/,
                options                 = null,
                optionDesc              = null,
                optionLabel             = null,
                parts;

            if ((parts = modifierRegex.exec(line)) !== null) {
              if (parts.index === modifierRegex.lastIndex) {
                modifierRegex.lastIndex++;
              }

              state       = [ parts[1], parts[6] ];
              optionLabel = parts[2];
              optionDesc  = parts[4].substring( 1, parts[4].length - 1 );
              options     = optionDesc.split('|');
            }

            return {
              name:               (state[0]) ? state[0].trim() : '',
              tagName:            (state[0]) ? state[0].trim().replace(' ','-').toLowerCase() : '',
              escaped:            (state[0]) ? state[0].trim().replace('.', ' ').replace(':', ' pseudo-class-') : '',
              description:        (state[1]) ? state[1].trim() : '',
              optionLabel:        optionLabel,
              optionDesc:         optionDesc,
              options:            options
            };
          }
        }
      }
    }
  }
};