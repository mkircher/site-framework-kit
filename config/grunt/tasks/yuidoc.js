module.exports.tasks = {
  yuidoc: {
    docs: {
      name: '<%= pkg.name %>',
      description: '<%= pkg.description %>',
      version: '<%= pkg.version %>',
      url: '<%= pkg.homepage %>',
      options: {
        paths: ['<%= paths.js %>'],
        outdir: '<%= paths.tmp %>/<%= paths.docs %>/js'
      }
    }
  }
};