module.exports = function(date, format){
	return moment(date).format(format);
};